package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

public class MainActivity extends AppCompatActivity {
    Button pickButton, addButton;
    ListView listView;
    EditText editText;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SharedPreferences myPref = getSharedPreferences("ACT", Context.MODE_PRIVATE);
        ArrayList<HashMap<String, Object>> actors = new ArrayList<>();

        String[] names = myPref.getString("names", "").split(",");

        for (int i = 0; i < names.length; i++){
            if (names[i].length() == 0){
                continue;
            }
            HashMap<String, Object> actor = new HashMap<>();
            actor.put("name", names[i]);
            actor.put("photo", Uri.parse(myPref.getString(names[i], "")));

            actors.add(actor);
        }



        pickButton = findViewById(R.id.button);
        addButton = findViewById(R.id.button2);
        listView = findViewById(R.id.list_view);
        editText = findViewById(R.id.editTextText);
        imageView = findViewById(R.id.imageView2);



        String[] from = {"name", "photo"};
        int[] to = {R.id.textView, R.id.imageView};
        SimpleAdapter simpleAdapter = new SimpleAdapter(this, actors, R.layout.item, from, to);
        listView.setAdapter(simpleAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String url = "https://www.google.com/search?q=" + actors.get(position).get("name");
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

        pickButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto , 1);
            }
        });
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = editText.getText().toString();
                if (text.length() == 0){
                    Toast.makeText(getApplicationContext(), "Фамилия не задана", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (path.length() == 0){
                    Toast.makeText(getApplicationContext(), "Фотография не выбрана", Toast.LENGTH_SHORT).show();
                    return;
                }
                HashMap<String, Object> actor = new HashMap<>();
                actor.put("name", text);
                actor.put("photo", Uri.parse(path));
                actors.add(actor);
                simpleAdapter.notifyDataSetChanged();

                SharedPreferences.Editor editor = myPref.edit();
                String names = "";
                for (int i = 0; i < actors.size(); i++){
                    names += actors.get(i).get("name") + ",";
                    editor.putString(actors.get(i).get("name").toString(), actors.get(i).get("photo").toString());
                }
                editor.putString("names", names);
                editor.commit();

            }
        });
    }

    String path = "";

    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        if (requestCode == 1){
            if(resultCode == RESULT_OK){
                Uri selectedImage = imageReturnedIntent.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(),
                            selectedImage);
                    float scaleWidth = (float) (300.0 / bitmap.getWidth());
                    float scaleHeight = (float) (300.0 / bitmap.getHeight());
                    Matrix matrix = new Matrix();
                    matrix.postScale(scaleWidth, scaleHeight);
                    Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
                            bitmap.getHeight(), matrix, false);
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                    path = MediaStore.Images.Media.insertImage(getApplicationContext().getContentResolver(),
                            resizedBitmap, String.valueOf(Math.random()), null);
                    imageView.setImageURI(Uri.parse(path));
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }
}